# VPC Peering and Transit Gateway
### Overview
[Amazon Virtual Private Cloud (Amazon VPC)](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html) enables you to launch AWS resources into a virtual network that you have defined.


#### VPC Peering
[Amazon VPC Peering](https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.htmlhttp:// "Amazon VPC Peering") enables the network connection between private VPCs giving the ability to route the traffic from one VPC to another. You can create VPC Peering between your own VPC with the VPC in the same region or a different region or with other VPCs in a different AWS account in a different region.

<p align="center">
   <img src="images/first-scenario-diagram.png" width="50%" height="50%">
</p>

#### Transit Gateway

[AWS Transit Gateway](https://aws.amazon.com/transit-gateway/) is a service that enables customers to connect their Amazon Virtual Private Clouds (VPCs) and their on-premises networks to a single gateway. 

With AWS Transit Gateway, you only have to create and manage a single connection from the central gateway in to each Amazon VPC, on-premises data center, or remote office across your network. 

<img src="https://miro.medium.com/max/1185/0*gF8kR72-YI73ztUy.png" width="50%">

## Scenarios

In this lab, you will learn how to create connections between your VPCs using VPC Peering and AWS Transit Gateway services.

## Prerequisites
> Prepare an AWS account. 

> Make sure your are in US East (N. Virginia), which short name is us-east-1.


## Step by Step

### a.	First Scenario (VPC Peering)

   <img src="images/images_peering-intro-diagram.png" width="40%">


#### Create VPC

1. On the **Services** menu, select **VPC**.

2. On the left navigation panel, select **Your VPCs**, then select **Create VPC**.

3. Enter the following information, then choose **Create**.
	- Name tag: `VPC_A`
	- IPv4 CIDR block: `10.0.0.0/16`

   <img src="images/CreateVPC-A.jpg" width="80%">

4. Create another VPC with the following information, then choose **Create**.
- Name tag: `VPC_B`
- IPv4 CIDR block: `172.16.0.0/16`

   <img src="images/CreateVPC-B.jpg" width="80%">

5. On the left navigation panel, select **Subnets**, then select **Create Subnet** , create 2 subnet for each VPC.

	#### Subnet VPC_A
	- Name tag: `VPC_A_Subnet`
	- VPC * : `VPC_A`
	- IPv4 CIDR Block* : `10.0.0.0/24`
	
	<img src="images/SubnetVPC_A.png" width="80%" height="80%">
   
	#### Subnet VPC_B
	- Name tag: `VPC_B_Subnet`
	- VPC * : `VPC_B`
	- IPv4 CIDR Block* : `172.16.0.0/24`

   <img src="images/SubnetVPC_B.png" width="80%" height="80%">
	
6. On the left navigation panel, select **Internet Gateways**, then select **Create Internet Gateway**
	- Name tag: `VPC_A_IGW`

   <img src="images/creategateway.png" width="80%" height="80%">
   
7. Right click on the new gateway and Attach to VPC

   <img src="images/AttachVPC.png" width="80%" height="80%">
   
8. On the left navigation panel, select **Route Tables**, then select **The Route table of VPC_A** and **Edit Route**.
	- Destination: `0.0.0.0/0`
	- Target : `VPC_A_IGW`
9. Add route and now VPC - A is connected to the internet.
	
	
------------


### Launch an EC2 Instance
[Launch an Elastic Compute Cloud](https://gitlab.com/ecloudture/knowledge-base/aws-sop/aws-elastic-compute-cloud/tree/master/Launch%20and%20Login%20EC2) Instance on each of the EC2 Instance to check connection between VPCs (There is no specific requirements of EC2 Instances on this lab, but I would recommend using **t2.micro** instance running the free tier **Amazon Linux 2** AMI


1. On the **Security group** of your instance [add a new rule](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-network-security.html#adding-security-group-rule)
	- Type: `Custom ICMP rule`
	- Protocol: `Echo Request`
	- Source: your choice (I would select Anywhere to be able to ping from any machine)


   <img src="images/pingrule.png" width="80%" height="80%">

2. After you launched the instances in each VPC - A and VPC - B , log in to your instances through SSH.


3. Try to ping your EC2 Instance in VPC_B from your the instance in your VPC_A
```
ping  x.x.x.x
```
> Do not forget to change the x.x.x.x into the Private IP Address of the EC2 Instance on your VPC_B

4. Results should return NOTHING as the two VPCs are not connected , Instance from VPC-A **cannot communicate** with the instance from VPC - B
5. We will solve this by creating a **Peering Connection**

-------

### Creating Peering Connection

1. On the **Services** menu, select **VPC**.
 
2. On the left navigation panel, select **Peering Connection**, then select **Create Peering Connection**

	- Peering connection name tag: `VPC_A->VPC_B`
	- VPC (Requester) : `VPC_A`
	- VPC (Accepter) : `VPC_B`

3. Choose **Create**
4. Once a pending request can be seen, **Accept Request**

	<img src="images/accept_vpc_request.png" width="50%" vspace="20" />
	
5. Now you can add the routes for all traffic directing to CIDR of VPC_B to go through the peering connection on the Route Tables.

	#### Route Table of VPC_A
	<img src="images/AddPeerA-B.png" width="100%">
	
	#### Route Table of VPC_B
	<img src="images/AddPeerB-A.png" width="100%">

6. Try to Ping again from your SSH Windows, now it should receive a respond from the instance as it is **successfully connected.**

	<img src="images/PeeringResults.jpg" width="70%" />
	


### Problems with VPC Peering

At a small scale, involving only a few VPCs, connectivity through Peering is “manageable” at best, but as you scale your network issues started, as VPC Peering requires each connection to be peered explicitly, which causes redundancy and inflexibility to your whole network.

<img src="images/vpc-peering-issues.png" width="100%" />

> Above pictures show as more VPCs are connected with each other, the more peering connections are needed to be created for the whole network.

### B.	Second Scenario (AWS Transit Gateway)

AWS Transit Gateway is designed to solve problems met while using VPC Peering regarding scalability as it offers a Simpler Design,it is a Fully Managed AWS Service,it is Cheaper for large scale networks, it increases network throughput, and offers flexible segmentation.


<img src="images/transitgateway.png" width="70%">


### Creating Transit Gateway

> Remember to delete your VPC Peering configuration before building your transit gateway network

1. On the VPC left navigation panel, select **Transit Gateway**, then select **Create Transit Gateway**, and wait until it has the status "Available"
	- Nametag : `Test-transitgateway`
	- Description : `Testing transit gateway services `

	<img src="images/transitgatewaycreate.png" width="70%">

2. On the VPC left navigation panel, select **Transit Gateway Attachment**, then select **Create Attachment**.
	- Transit Gateway ID* : `(Test-transitgateway ID)`
	- Attachment nametag : `VPC_A Attach`
	- VPC ID* : `(VPC_A ID)`
	
	<img src="images/attacha.png" width="70%">

3. On the VPC left navigation panel, select **Transit Gateway Attachment**, then select **Create Attachment**.
	- Transit Gateway ID* : `(Test-transitgateway ID)`
	- Attachment nametag : `VPC_B Attach`
	- VPC ID* : `(VPC_B ID)`
	
	<img src="images/attach.png" width="70%">
	
	

4. On the VPC left navigation panel, select **Transit Gateway Route Tables**, in here you can see every attachments and routes configured for your Transit Gateway
	
	<img src="images/tgatewaytable.png" width="100%">
	
5. Last step would be to reroute your Internet Gateway on VPC_A to our `Test-transitgateway`
	
	<img src="images/laststep.png" width="100%">

	> Remember to do it for both VPC - A and VPC - B
		
6. Now both of the VPC are able to connect with each other, but the network has no connection to internet, to fix this create a new **Core VPC** that we will attach to internet gateway
	- Name tag : `Core VPC`
	- CIDR : `192.168.0.0/16`

	Also add another subnet for your core VPC ( On the VPC left navigation panel, select **Subnets**) and **Create Subnet**.
	
	- Name tag : `Core VPC Public`
	- VPC : `Core VPC`
	- Availability Zones : `us-east-1a`
	- CIDR : `192.168.0.0/16`

7.  On the VPC left navigation panel, select **Internet Gateway** and Attach the former Internet Gateway that we used to the **Core VPC**

8. On the VPC left navigation panel, select **Transit Gateway Attachment**, then select **Create Attachment**.
	- Transit Gateway ID* : `(Test-transitgateway ID)`
	- Attachment nametag : `CORE VPC Attach`
	- VPC ID* : `(CORE VPC ID)`
	
	<img src="images/attacha.png" width="70%">

9.  On the VPC left navigation panel, select **Transit Gateway Route Tables**, in here add a new route, routing all address to connect to the CORE VPC which is connected to the internet

	- CIDR : `0.0.0.0/0`
	- Choose Attachment : `(CORE VPC Attach ID)`
	<img src="images/corevpc.png" width="100%">

10. Now your transit gateway network has access to internet.

11. **OPTIONAL** ** Finish our network structure by adding VPC - C and VPC - D to our network.

## Conclusion

CONGRATULATIONS YOU HAVE LEARNED!

1. How to connect VPCs with VPC Peering
2. How to manage Large Scale VPC Connection with Transit Gateway
3. When to use VPC Peering and Transit Gateway
4. Difference between VPC Peering and Transit Gateway

## Resource Clearing

1. Terminate ALl EC2 Instances
2. Delete All Transit Gateway Attachments ,then Transit Gateway
3. Delete All VPCs

## Reference
* [Amazon Virtual Private Cloud (Amazon VPC)](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html)
* [Amazon VPC Peering](https://docs.aws.amazon.com/vpc/latest/peering/what-is-vpc-peering.htmlhttp:// "Amazon VPC Peering")
* [AWS Transit Gateway](https://aws.amazon.com/transit-gateway/) 
* [Launch an Elastic Compute Cloud](https://gitlab.com/ecloudture/knowledge-base/aws-sop/aws-elastic-compute-cloud/tree/master/Launch%20and%20Login%20EC2)
